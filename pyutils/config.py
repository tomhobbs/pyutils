from pyutils import exceptions

import logging
import os
import simplejson as json

logger = logging.getLogger(__name__)

env = os.environ.get('APP_ENV')
if env is None:
    raise exceptions.ConfigurationException('APP_ENV not set')

cfg = None


def init(file):
    """
    Expects a JSON file that can be accessed and read in.  Further, this JSON
    file should have a top-level element whose name matches the value of
    APP_ENV.  This allows multiple configuration options within the same file.

    e.g. The file might look like;
    {
      "development": {
        "db_host": "localhost",
        "db_port": 30170,
        "db_user": "tom",
        "db_pass": "tom"
      },
      "test": {
        "db_host": "jenkins_build",
        "db_port": 20175,
        ... etc
      },
    }
    :param file: Path to the JSON configuration
    :return: nothing
    """
    global cfg
    with open(file, 'r') as cfg_file:
        cfg = json.load(cfg_file)

    try:
        cfg = cfg[env]
    except KeyError as e:
        raise exceptions.ConfigurationException(f'No config found to match APP_ENV={env}')


def get(*keys, or_else=None):
    """
    Where keys form the path through the config file to the item.  It tolerates
    missing config by returning None or the default value
    :param keys: XPath-esc route from APP_ENV to the target value
    :param or_else: The default value if the config item is not present
    :return: The value from the config file, or the default value
    """
    global cfg
    if cfg is None:
        logger.warning('No configuration loaded')
        return None

    obj = cfg
    for key in keys:
        logger.debug(f'Fetching {key}')
        try:
            obj = obj[key]
        except KeyError:
            logger.debug(f'No key: {key} in {obj}')
            return or_else
    return obj


def get_int(*keys, or_else=0):
    v = get(*keys, or_else=or_else)
    return int(v)


def strict_get(*keys):
    """
    As get, but raises MissingConfigurationException if the value cannot be
    found in the loaded config
    :param keys: XPath-esc route from APP_ENV to the target value
    :return: The value from the config file
    """
    global env, cfg
    val = get(*keys)
    if val is None:
        full_path = env + '/' + '/'.join(keys)
        raise exceptions.MissingConfigurationException(f'No value found for {full_path}')
    else:
        return val


def strict_get_int(*keys):
    v = get(*keys)
    return int(v)
