class ConfigurationException(Exception):
    pass


class MissingConfigurationException(Exception):
    pass


class ArgumentException(Exception):
    pass
