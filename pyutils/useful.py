import logging
import time
import datetime
from functools import wraps


def current_time_millis():
    return int(round(time.time() * 1000))


def millis_to_datetime(ms):
    s = ms / 1000.0
    return datetime.datetime.fromtimestamp(s)


def datetime_to_timestamp_str(d):
    if d is None:
        return None
    else:
        return d.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]


def log_entry_exit(logger):
    """
    Decorator which accepts a logger and writes entry/exit and duration stats
    around the function that it has decorated.
    Makes no promises about performance...
    :param logger:
    :return:
    """
    def decorator(func):
        @wraps(func)
        def decorated(*args, **kwargs):
            logger.info(f'Enter: {func.__name__}')
            start = current_time_millis()
            resp = func(*args, **kwargs)
            duration = current_time_millis() - start
            logger.info(f'Exit: {func.__name__} took {duration}ms')
            return resp
        return decorated
    return decorator
