import logging

TRACE_LEVEL = 5
logging.addLevelName(TRACE_LEVEL, "TRACE")


def __log_at_trace(self, message, *args, **kwargs):
    if self.isEnabledFor(TRACE_LEVEL):
        self._log(TRACE_LEVEL, message, args, kwargs)


def allow():
    """
    Enabled a trace function on the standard logger
    :return: Nothing
    """
    logging.Logger.trace = __log_at_trace
