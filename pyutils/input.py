import logging

logger = logging.getLogger(__name__)


def truish(x):
    if x is None:
        return False
    elif type(x) is bool:
        return x
    elif isinstance(x, str):
        return x.lower() in ('true', 't', 'y', 'yes')
    else:
        return False


def int_or_else(x, or_else):
    if x is None:
        return or_else
    try:
        return int(x)
    except ValueError:
        logger.debug(f'Value not an int: \"{x}\"')
        return or_else
