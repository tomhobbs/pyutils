from functools import wraps
import traceback


def http_response(transform):
    """
    Decorator that looks at the response (expected to be a dict or list) and
    attempts to guess an appropriate HTTP response code.  This is all pacakged
    up and returned nicely for the HTTP server in use.
    The docorators single function argument should be a function that can
    transform the triple this decorator creates into an appropriate response
    that the server knows how to deal with, e.g. converting it into a Flask
    Response.
    :param transform: None, a dict or a list
    :return: {
        'status': int,
        'data': decorated function return,
        'mimetype: 'application/json; charset=utf-8'
    }
    """
    def http_code_from_response(r):
        if r is None:
            return 404
        elif isinstance(r, list):
            if 0 == len(r):
                return 204
            else:
                return 200
        else:
            if 'status' in r:
                return r['status']
            elif 'code' in r:
                return r['code']
            elif 'errorCode' in r:
                return r['errorCode']
            elif 'eCode' in r:
                return r['eCode']
            else:
                # Then everything was probably fine
                return 200

    def decorator(func):
        @wraps(func)
        def decorated(*args, **kwargs):
            try:
                resp = func(*args, **kwargs)
                code = http_code_from_response(resp)
                return transform({
                    'data': resp,
                    'status': code,
                    'mimetype': 'application/json; charset=utf-8'
                })
            except Exception as e:
                traceback.print_exc()
                return transform({
                    'data': {
                        'errorCode': 500,
                        'errorMessage': str(e)
                    },
                    'status': 500,
                    'mimetype': 'application/json; charset=utf-8'
                })
        return decorated
    return decorator
