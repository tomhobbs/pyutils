# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='pyutils',
    version='0.0.1',
    description='Custom PyUtils',
    long_description=long_description,
    url='https://bitbucket.org/tomhobbs/pyutils',
    author='Tom Hobbs',
    author_email='tvhobbs@googlemail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='common custom utils',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=['simplejson'],
    extras_require={},
    package_data={},
    entry_points={}
)
