# PyUtils

My own util library.

# Credits

Adapted the pip setup help from [stevenferreira](https://gemfury.com/stevenferreira/python:sample)

# License

MIT, see LICENSE
